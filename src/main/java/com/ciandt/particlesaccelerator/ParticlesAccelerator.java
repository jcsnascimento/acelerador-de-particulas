package com.ciandt.particlesaccelerator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ciandt.particlesaccelerator.exceptions.ExceedAtoms;
import com.ciandt.particlesaccelerator.exceptions.SizeOrRegionException;

public class ParticlesAccelerator {

	/**
	 * function to get printable string of accelerator
	 * 
	 * @param arrRegions array with all regions
	 * @return 
	 */
	private String printAccelerator(List<Integer> arrRegions) {
		return arrRegions.stream().map(n -> String.valueOf(n)).collect(Collectors.joining(" "));
	}
	
	/*
	 * function to initialize the accelerator
	 */
	private List<Integer> acceleratorInitialize(int regions) {
		List<Integer> arrRegions = new ArrayList<Integer>(regions);

		// regions initialize
		for (int currentRegion = 0; currentRegion < regions; currentRegion++) {
			arrRegions.add(0);
		}
		
		return arrRegions;
	}

	/**
	 * function to get distribution string
	 * 
	 * @param atoms long number of atoms to accelerate
	 * @param size  int size of regions
	 * @param regions int number of regions
	 * 
	 * @return printable string
	 *  
	 * @throws ExceedAtoms
	 * @throws SizeOrRegionException
	 */
	public String getDistribution(long atoms, int size, int regions) throws ExceedAtoms, SizeOrRegionException {

		if (atoms > 1000000000) {
			throw new ExceedAtoms("Atoms must not exceed 1000000000");
		}
		if (size > 100 || regions > 100) {
			throw new SizeOrRegionException("Size or Regions must not exceed 100");
		}

		List<Integer> arrRegions = this.acceleratorInitialize(regions);

		for (long i = 1; i <= atoms; i++) {
			boolean inserted = false;
			for (int currentRegion = 0; currentRegion <= regions; currentRegion++) {
				int region = arrRegions.get(currentRegion);
				if (region <= size - 1 && !inserted) {
					arrRegions.set(currentRegion, region + 1);
					inserted = true;
					break;
				} else if (region == size && !inserted) {
					arrRegions.set(currentRegion, 0);
				}
			}
		}

		return printAccelerator(arrRegions);
	}

	/**
	 * default constructor
	 */
	public ParticlesAccelerator() {
		
	}
	
	
	
	/**
	 * "Entrada A entrada consiste em uma linha contendo os números A, N e K
	 * separados por espaço. O acelerador começa vazio, ou seja, com átomos em
	 * cada um de seus K níveis."
	 * 
	 * @throws SizeOrRegionException 
	 * @throws ExceedAtoms 
	 * 
	 */
	public String getDistribution(String args) throws ExceedAtoms, SizeOrRegionException {
		String[] inputs = args.split(" ");

		long atoms = Long.parseLong(inputs[0]);
		int size = Integer.parseInt(inputs[1]);
		int regions = Integer.parseInt(inputs[2]);
		
		ParticlesAccelerator accelerator = new ParticlesAccelerator();
		
		return accelerator.getDistribution(atoms, size, regions);
		
	}

}
