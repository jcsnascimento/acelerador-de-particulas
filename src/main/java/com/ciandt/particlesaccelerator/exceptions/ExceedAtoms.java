package com.ciandt.particlesaccelerator.exceptions;

public class ExceedAtoms extends Exception {

	public ExceedAtoms(String message) {
		super(message);
	}
}
