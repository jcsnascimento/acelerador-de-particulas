package com.ciandt.particlesaccelerator.exceptions;

public class SizeOrRegionException extends Exception {
	
	public SizeOrRegionException(String message) {
		super(message);
	}
}
