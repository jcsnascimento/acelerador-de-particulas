package com.ciandt.particlesaccelerator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ciandt.particlesaccelerator.ParticlesAccelerator;
import com.ciandt.particlesaccelerator.exceptions.ExceedAtoms;
import com.ciandt.particlesaccelerator.exceptions.SizeOrRegionException;

public class ParticlesAcceleratorTest {
	
	private ParticlesAccelerator accelerator = new ParticlesAccelerator();

	@Test
	public void teste1() throws ExceedAtoms, SizeOrRegionException {
		assertEquals("1 1 0", accelerator.getDistribution(3, 1, 3));
	}

	@Test
	public void teste2() throws ExceedAtoms, SizeOrRegionException {
		assertEquals("10 61 59 61 9 0 0 0 0 0", accelerator.getDistribution(1_000_000_000, 100, 10));
	}

	@Test(expected=ExceedAtoms.class)
	public void teste3() throws ExceedAtoms, SizeOrRegionException {
		assertEquals("10 61 59 61 9 0 0 0 0 0", accelerator.getDistribution(1_000_000_001, 100, 10));
	}

	@Test
	public void teste4() throws ExceedAtoms, SizeOrRegionException {
		assertEquals("1 0 0 0 1 0 1 2 0 0 0 2 0 0 2 0 2 1 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
				accelerator.getDistribution(1000_000_000, 2, 50));
	}

	@Test(expected=SizeOrRegionException.class)
	public void teste5() throws ExceedAtoms, SizeOrRegionException {
		assertEquals("91 9 0 0 0 0 0 0 0 0", accelerator.getDistribution(1_000, 101, 10));
	}

	@Test(expected=SizeOrRegionException.class)
	public void teste6() throws ExceedAtoms, SizeOrRegionException {
		assertEquals(
				"10 2 8 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
				accelerator.getDistribution(1_000, 10, 101));
	}
	
	@Test
	public void testeDoJulio() throws ExceedAtoms, SizeOrRegionException {
		assertEquals("10 61 59 61 9 0 0 0 0 0", accelerator.getDistribution("1000000000 100 10"));
	}

}
